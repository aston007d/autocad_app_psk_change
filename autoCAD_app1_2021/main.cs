﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;
using System.Reflection;

namespace autoCAD_app1_2021
{
    /// <summary>
    /// Расширение для перевода в радианы и градусы
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        /// радианы в градусы с округлением до 
        /// </summary>
        /// <param name="radians">Значение в радианах</param>
        /// <returns></returns>
        public static double RadiansToDegrees(this double radians)
        {
            return (180 / Math.PI) * radians;
        }
        /// <summary>
        /// градусы в радианы с округлением до 
        /// </summary>
        /// <param name="degrees">значение в градусах</param>
        /// <returns></returns>
        public static double DegreesToRadians(this double degrees)
        {
            return (Math.PI / 180) * degrees;
        }

    }
    public class main : IExtensionApplication
    {
        [CommandMethod("PSK_change")]
        public static void psk_change()
        {
            #region набор переменных
            //-------------------------------------------------------------------------
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor; // это командная строка автокада
            List<entity_ucs> list_obj_USC = new List<entity_ucs>(); // список всех ПСК
            entity_ucs.list_names_USC.Clear(); // очистка перед запуском программы. 
            entity_ucs.list_selected_objs.Clear(); // очистка перед запуском программы. 
            #endregion
            //-------------------------------------------------------------------------
            #region Просит пользователя выбрать объекты 
            // Start a transaction
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                // Request for objects to be selected in the drawing area
                PromptSelectionResult acSSPrompt = doc.Editor.GetSelection();
                // If the prompt status is OK, objects were selected
                if (acSSPrompt.Status == PromptStatus.OK)
                {
                    SelectionSet currentlySelectedEntities = acSSPrompt.Value;
                    foreach (ObjectId id in currentlySelectedEntities.GetObjectIds())
                    {
                        entity_ucs.list_selected_objs.Add(id); // добавить в список id выбранных пользователем объектов
                    }


                    // Save the new object to the database
                    acTrans.Commit();
                }
                // Dispose of the transaction
            }
            #endregion

            if (entity_ucs.list_selected_objs.Count == 0)
            {
                Autodesk.AutoCAD.ApplicationServices.Application.ShowAlertDialog("Не выбрано ни одного объекта. " +
                    "Попробуйте снова:\n ");
                return;
            }
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                // Open the UCS table for read
                UcsTable acUCSTbl;
                acUCSTbl = trans.GetObject(db.UcsTableId, OpenMode.ForRead) as UcsTable;
                //UcsTableRecord acUCSTblRec;
                foreach (ObjectId id in acUCSTbl)
                {
                    UcsTableRecord current_ucs = (UcsTableRecord)trans.GetObject(id, OpenMode.ForRead);
                    Point3d origin_point_of_UCS = current_ucs.Origin;
                    string name_UCS = current_ucs.Name;
                    entity_ucs.list_names_USC.Add(name_UCS);
                    Vector3d Xaxis = current_ucs.XAxis;
                    double angel = (new Vector3d(1, 0, 0)).GetAngleTo(Xaxis); // угол между МСК и ПСК в 2д
                    entity_ucs obj = new entity_ucs(origin_point_of_UCS, name_UCS, angel);
                    list_obj_USC.Add(obj); // НАЙДЕННЫЕ ПСК ДОБАВЛЯЕМ В ВИДЕ ОБЪЕКТОВ КЛАССА entity_ucs В СПИСОК
                }
                trans.Commit();
                Form1 form1 = new Form1(list_obj_USC, doc, db, ed);
                form1.Show();
                
                
            }

        }

        public void Initialize()
        {
            var editor = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            editor.WriteMessage("Инициализация плагина.." + Environment.NewLine);

        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }

    }

}
