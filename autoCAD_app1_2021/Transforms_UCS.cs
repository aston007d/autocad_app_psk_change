﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using System.Windows.Forms;

namespace autoCAD_app1_2021
{
    public static class Transforms_UCS
    {
        public static void Transform(Document doc, Database db, string name_UCS, double scale_value)
        {
            using (DocumentLock acLckDoc = doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    try
                    {
                        //создаем вершину
                        Point3d vertex_test_UCS1 = new Point3d(1, 1, 0);
                        // добавим переменную консоли
                        Editor editor = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
                        #region Получение текущей ПСК
                        Matrix3d UCS1 = editor.CurrentUserCoordinateSystem;
                        #endregion

                        #region Получение выбранным пользователем ПСК в программе
                        // Открываем UCS(ПСК) таблицу на чтение
                        UcsTable acUCSTbl = trans.GetObject(db.UcsTableId, OpenMode.ForRead) as UcsTable;
                        // проверяем существует ли она
                        if (acUCSTbl.Has(name_UCS) == false)
                        {
                            editor.WriteMessage($"Не найден ПСК с именем {name_UCS}");
                            return;
                        }
                        UcsTableRecord acUCSTblRec = trans.GetObject(acUCSTbl[name_UCS], OpenMode.ForRead) as UcsTableRecord;

                        // далее в активном вьюпорте ставим ПСК выбранную
                        //открываем вьюпорт
                        ViewportTableRecord acVportTblRec;
                        acVportTblRec = trans.GetObject(doc.Editor.ActiveViewportId, OpenMode.ForWrite) as ViewportTableRecord;
                        // Display the UCS Icon at the origin of the current viewport
                        acVportTblRec.IconAtOrigin = true;
                        acVportTblRec.IconEnabled = true;
                        // Set the UCS current
                        acVportTblRec.SetUcs(acUCSTblRec.ObjectId);
                        doc.Editor.UpdateTiledViewportsFromDatabase();

                        //Получаем ПСК2 - ту, которая была выбрана пользователем 
                        Matrix3d UCS2 = editor.CurrentUserCoordinateSystem;
                        
                        // Display the name of the current UCS           
                        UcsTableRecord acUCSTblRecActive = trans.GetObject(acVportTblRec.UcsName, OpenMode.ForRead) as UcsTableRecord;
                        //Autodesk.AutoCAD.ApplicationServices.Application.ShowAlertDialog("Текущая ПСК: " +
                        //                                  acUCSTblRecActive.Name);
                        #endregion

                        #region Перемещение точки в из одной ПСК в дргую ПСК
                        CoordinateSystem3d UCS_coord1 = UCS1.CoordinateSystem3d;
                        CoordinateSystem3d UCS_coord2 = UCS2.CoordinateSystem3d;
                        // Transform from UCS to WCS
                        Matrix3d mat =
                            Matrix3d.AlignCoordinateSystem(
                            UCS_coord1.Origin,
                            UCS_coord1.Xaxis,
                            UCS_coord1.Yaxis,
                            UCS_coord1.Zaxis,
                            UCS_coord2.Origin,
                            UCS_coord2.Xaxis,
                            UCS_coord2.Yaxis,
                            UCS_coord2.Zaxis
                            );

                        //ObjectId id1 = entity_ucs.list_selected_objs.First();
                        foreach (ObjectId id in entity_ucs.list_selected_objs)
                        {
                            Entity ent = trans.GetObject(id, OpenMode.ForWrite) as Entity;
                            ent.TransformBy(mat);
                        }

                        foreach (ObjectId id in entity_ucs.list_selected_objs)
                        {
                            Entity ent = trans.GetObject(id, OpenMode.ForWrite) as Entity;
                            ent.TransformBy(Matrix3d.Scaling(scale_value, new Point3d(0, 0, 0)));
                        }




                        #endregion

                        trans.Commit();
                    }
                    catch (System.Exception ex)
                    {
                        trans.Abort();
                        Autodesk.AutoCAD.ApplicationServices.Application.ShowAlertDialog("Ошибка:\n " + ex.ToString());
                    }
                }
            }
           
        }

        public static void new_ucs(string name_of_UCS,double X, double Y, double angel)
        {
            Point3d origin = new Point3d(X, Y, 0);
            Vector3d Zaxis = new Vector3d(0, 0, 1);
            Vector3d Xaxis = (new Vector3d(1, 0, 0)).RotateBy(angel, Zaxis); // повернуть вектор на угол
            Vector3d Yaxis = new Vector3d(0, 1, 0).RotateBy(angel, Zaxis); // повернуть вектор на угол

            // Get the current document and database, and start a transaction
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database acCurDb = doc.Database;

            using (DocumentLock acLckDoc = doc.LockDocument())
            {
                
                using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
                {
                    // Open the UCS table for read
                    UcsTable acUCSTbl;
                    acUCSTbl = acTrans.GetObject(acCurDb.UcsTableId, OpenMode.ForRead) as UcsTable;
                    UcsTableRecord acUCSTblRec;
                    // Check to see if the "New_UCS" UCS table record exists
                    if (acUCSTbl.Has(name_of_UCS) == false)
                    {
                        acUCSTblRec = new UcsTableRecord();                        
                        acUCSTblRec.Name = name_of_UCS;
                        // Open the UCSTable for write
                        acUCSTbl.UpgradeOpen();
                        // Add the new UCS table record
                        acUCSTbl.Add(acUCSTblRec);
                        acTrans.AddNewlyCreatedDBObject(acUCSTblRec, true);
                    }
                    else
                    {
                        acUCSTblRec = acTrans.GetObject(acUCSTbl[name_of_UCS], OpenMode.ForWrite) as UcsTableRecord;
                    }
                    
                    acUCSTblRec.Origin = origin;
                    acUCSTblRec.XAxis = Xaxis;
                    acUCSTblRec.YAxis = Yaxis;
                    // Open the active viewport
                    ViewportTableRecord acVportTblRec;
                    acVportTblRec = acTrans.GetObject(doc.Editor.ActiveViewportId,OpenMode.ForWrite) as ViewportTableRecord;
                    // Display the UCS Icon at the origin of the current viewport
                    acVportTblRec.IconAtOrigin = true;
                    acVportTblRec.IconEnabled = true;
                    // Set the UCS current
                    acVportTblRec.SetUcs(acUCSTblRec.ObjectId);
                    doc.Editor.UpdateTiledViewportsFromDatabase();
                    // Display the name of the current UCS
                    UcsTableRecord acUCSTblRecActive;
                    acUCSTblRecActive = acTrans.GetObject(acVportTblRec.UcsName, OpenMode.ForRead) as UcsTableRecord;
                    //Autodesk.AutoCAD.ApplicationServices.Application.ShowAlertDialog("The current UCS is: " + acUCSTblRecActive.Name);

                    // Save the new objects to the database
                    acTrans.Commit();
                    MessageBox.Show($"Данные успешно добавлены для ПСК с именем {name_of_UCS}");
                }
            }
                

        }

        public static void delete_ucs(string name_of_UCS)
        {
            // Get the current document and database, and start a transaction
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database acCurDb = doc.Database;
            using (DocumentLock acLckDoc = doc.LockDocument())
            {
                
                using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
                {
                    // Open the UCS table for read
                    UcsTable acUCSTbl;
                    acUCSTbl = acTrans.GetObject(acCurDb.UcsTableId, OpenMode.ForRead) as UcsTable;
                    UcsTableRecord acUCSTblRec;
                    // Check to see if the "New_UCS" UCS table record exists
                    if (acUCSTbl.Has(name_of_UCS) == true)
                    {
                        acUCSTblRec = acTrans.GetObject(acUCSTbl[name_of_UCS], OpenMode.ForWrite) as UcsTableRecord;
                        acUCSTblRec.Erase();
                        acTrans.Commit();
                    }
                    else
                    {
                        acTrans.Abort();
                    }
                }
            }
        }
    }
}
