﻿namespace autoCAD_app1_2021
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.textbox_name_ucs = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_angel = new System.Windows.Forms.TextBox();
            this.textBox_north = new System.Windows.Forms.TextBox();
            this.textBox_east = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(18, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "Название";
            // 
            // textbox_name_ucs
            // 
            this.textbox_name_ucs.Enabled = false;
            this.textbox_name_ucs.Location = new System.Drawing.Point(18, 49);
            this.textbox_name_ucs.Name = "textbox_name_ucs";
            this.textbox_name_ucs.Size = new System.Drawing.Size(100, 20);
            this.textbox_name_ucs.TabIndex = 27;
            this.textbox_name_ucs.TextChanged += new System.EventHandler(this.textbox_name_ucs_TextChanged);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(190, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 26;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(246, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Север";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(128, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 23;
            this.label1.Text = "Восток";
            // 
            // textBox_angel
            // 
            this.textBox_angel.Location = new System.Drawing.Point(356, 49);
            this.textBox_angel.Name = "textBox_angel";
            this.textBox_angel.Size = new System.Drawing.Size(100, 20);
            this.textBox_angel.TabIndex = 22;
            this.textBox_angel.TextChanged += new System.EventHandler(this.textBox_angel_TextChanged);
            this.textBox_angel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_angel_KeyPress);
            // 
            // textBox_north
            // 
            this.textBox_north.Location = new System.Drawing.Point(243, 49);
            this.textBox_north.Name = "textBox_north";
            this.textBox_north.Size = new System.Drawing.Size(100, 20);
            this.textBox_north.TabIndex = 21;
            this.textBox_north.TextChanged += new System.EventHandler(this.textBox_north_TextChanged);
            this.textBox_north.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_north_KeyPress);
            // 
            // textBox_east
            // 
            this.textBox_east.Location = new System.Drawing.Point(128, 49);
            this.textBox_east.Name = "textBox_east";
            this.textBox_east.Size = new System.Drawing.Size(100, 20);
            this.textBox_east.TabIndex = 20;
            this.textBox_east.TextChanged += new System.EventHandler(this.textBox_east_TextChanged);
            this.textBox_east.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_east_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(353, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "Поворот, град°";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 124);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textbox_name_ucs);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_angel);
            this.Controls.Add(this.textBox_north);
            this.Controls.Add(this.textBox_east);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Изменить ПСК";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textbox_name_ucs;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_angel;
        private System.Windows.Forms.TextBox textBox_north;
        private System.Windows.Forms.TextBox textBox_east;
        private System.Windows.Forms.Label label3;
    }
}