﻿namespace autoCAD_app1_2021
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_angel = new System.Windows.Forms.TextBox();
            this.textBox_north = new System.Windows.Forms.TextBox();
            this.textBox_east = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textbox_name_ucs = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(240, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Север";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(122, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Восток";
            // 
            // textBox_angel
            // 
            this.textBox_angel.Location = new System.Drawing.Point(350, 43);
            this.textBox_angel.Name = "textBox_angel";
            this.textBox_angel.Size = new System.Drawing.Size(100, 20);
            this.textBox_angel.TabIndex = 13;
            this.textBox_angel.TextChanged += new System.EventHandler(this.textBox_angel_TextChanged);
            this.textBox_angel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_angel_KeyPress);
            // 
            // textBox_north
            // 
            this.textBox_north.Location = new System.Drawing.Point(237, 43);
            this.textBox_north.Name = "textBox_north";
            this.textBox_north.Size = new System.Drawing.Size(100, 20);
            this.textBox_north.TabIndex = 12;
            this.textBox_north.TextChanged += new System.EventHandler(this.textBox_north_TextChanged);
            this.textBox_north.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_north_KeyPress);
            // 
            // textBox_east
            // 
            this.textBox_east.Location = new System.Drawing.Point(122, 43);
            this.textBox_east.Name = "textBox_east";
            this.textBox_east.Size = new System.Drawing.Size(100, 20);
            this.textBox_east.TabIndex = 11;
            this.textBox_east.TextChanged += new System.EventHandler(this.textBox_east_TextChanged);
            this.textBox_east.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_east_KeyPress);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(184, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textbox_name_ucs
            // 
            this.textbox_name_ucs.Location = new System.Drawing.Point(12, 43);
            this.textbox_name_ucs.Name = "textbox_name_ucs";
            this.textbox_name_ucs.Size = new System.Drawing.Size(100, 20);
            this.textbox_name_ucs.TabIndex = 18;
            this.textbox_name_ucs.TextChanged += new System.EventHandler(this.textbox_name_ucs_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Название";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(347, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Поворот, град°";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 114);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textbox_name_ucs);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_angel);
            this.Controls.Add(this.textBox_north);
            this.Controls.Add(this.textBox_east);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление ПСК";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_angel;
        private System.Windows.Forms.TextBox textBox_north;
        private System.Windows.Forms.TextBox textBox_east;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textbox_name_ucs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}