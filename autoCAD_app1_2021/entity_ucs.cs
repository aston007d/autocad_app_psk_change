﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;



namespace autoCAD_app1_2021
{
    public class entity_ucs
    {
        static public List<string> list_names_USC = new List<string>(); // список всех ПСК
        static public List<ObjectId> list_selected_objs = new List<ObjectId>(); // список выбранных объектов
        internal string names_USC { get; set; } // имя ПСК
        internal Point3d origin_point_of_UCS { get; set; } // точка распололжения центра ПСК
        internal double angel { get; set; } // угол поворота сист коорд. против часовой оси в радианах
        /// <summary>
        /// 
        /// </summary>
        /// <param name="origin_point_of_UCS">центр новой ПСК</param>
        /// <param name="names_USC">имя новой ПСК</param>
        /// <param name="angel">угол поворота новой ПСК относительно МСК</param>
        public entity_ucs(Point3d origin_point_of_UCS, string names_USC,double angel)
        {
            this.origin_point_of_UCS = origin_point_of_UCS;
            this.names_USC = names_USC;
            this.angel = angel;
        }

    }
}
