﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace autoCAD_app1_2021
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string name_ucs = textbox_name_ucs.Text;
                //string name_ucs = "DIMON";
                double east = Double.Parse(textBox_east.Text);
                double north = Double.Parse(textBox_north.Text);
                double angel = Double.Parse(textBox_angel.Text).DegreesToRadians();
                Transforms_UCS.new_ucs(name_ucs, east, north, angel);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                this.Close();
            }
            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox_east_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 46) //цифры, клавиша BackSpace и точка а ASCII
            {
                e.Handled = true;
            }
        }

        private void textBox_north_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 46) //цифры, клавиша BackSpace и точка а ASCII
            {
                e.Handled = true;
            }
        }

        private void textBox_angel_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 ) //цифры, клавиша BackSpace ASCII
            {
                e.Handled = true;
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void textbox_name_ucs_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = this.Controls.OfType<TextBox>().All(x => x.Text.Length > 0);
        }

        private void textBox_east_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = this.Controls.OfType<TextBox>().All(x => x.Text.Length > 0);
        }

        private void textBox_north_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = this.Controls.OfType<TextBox>().All(x => x.Text.Length > 0);
        }

        private void textBox_angel_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = this.Controls.OfType<TextBox>().All(x => x.Text.Length > 0);
        }
    }
}
