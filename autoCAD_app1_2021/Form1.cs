﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Exception = System.Exception;

namespace autoCAD_app1_2021
{
    
    public partial class Form1 : Form
    {
        List<entity_ucs> List_obj_USC; // список entity_ucs, содержит информацию о всех ПСК найденных
        Document doc;
        Database db;
        Editor ed; // это командная строка автокада
       
        public Form1(List<entity_ucs> list_obj_USC, Document doc, Database db, Editor ed)
        {
            List_obj_USC = list_obj_USC;
            InitializeComponent();
            comboBox1.DataSource = entity_ucs.list_names_USC;
            this.doc = doc;
            this.db = db;
            this.ed = ed;
            toolTip1.SetToolTip(textBoxScale, "Масштаб при миграции в новую ПСК");

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string selectedUCS = (string)comboBox1.SelectedItem;
            Form3 form3 = new Form3(selectedUCS, textBox1.Text,textBox2.Text,textBox3.Text);
            form3.ShowDialog();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;

            // Save the selected employee's name, because we will remove
            // the employee's name from the list.
            string selectedEmployee = (string)comboBox1.SelectedItem;
            entity_ucs choosen_UCS =  List_obj_USC.Find(x => x.names_USC == selectedEmployee);
            textBox1.Text = (Math.Round(choosen_UCS.origin_point_of_UCS.X,2)).ToString();
            textBox2.Text = (Math.Round(choosen_UCS.origin_point_of_UCS.Y,2)).ToString();
            textBox3.Text = (Math.Round(choosen_UCS.angel.RadiansToDegrees(),0)).ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try 
            {
                string choosen_UCS = comboBox1.SelectedItem.ToString();
                double scale_value = textBoxScale.Text == "" ? 1 :  Double.Parse(textBoxScale.Text);
                this.Close();
                Transforms_UCS.Transform(doc, db, choosen_UCS, scale_value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

           

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();            
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try 
            {
                string name_ucs = comboBox1.SelectedItem.ToString();
                DialogResult result = MessageBox.Show($"Удалить ПСК с именем {name_ucs}?",
            "Предупреждение",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);

                if (result == DialogResult.Yes)
                {
                    Transforms_UCS.delete_ucs(name_ucs);
                    MessageBox.Show("Успешно удалено");
                    this.Close();
                    //ДОПИСАТЬ ОБНОВЛЕННИЯ СПИСКА ПСК
                }

                else
                {

                }
            }
            catch(System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 46) //цифры, клавиша BackSpace и точка а ASCII
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 46) //цифры, клавиша BackSpace и точка а ASCII
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 ) //цифры, клавиша BackSpace ASCII
            {
                e.Handled = true;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8) //цифры, клавиша BackSpace ASCII
            {
                e.Handled = true;
            }
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
    }
}
